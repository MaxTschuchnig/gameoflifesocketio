var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const GameOfLife = require('life-game');

/*
 *
 *	For Map
 *
 */

// List for maps
maps = [];

// Map boundings
var width = 50;
var height = 50;

/*
 *
 *	For Socket
 *
 */

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
	console.log('client ' + socket.id + ' connected');
	io.to(socket.id).emit('message', 'client ' + socket.id + ' connected');
	
	generate_new_map(socket.id);
	
	socket.on('map', function(msg){
		temp = get_current_map(socket.id).map.map;
		temp = get_minimap(temp);
		io.emit('map', JSON.stringify(temp));
	});
	
	socket.on('increment', function(msg){
		increment_map(socket.id);
	});
});

http.listen(3000, function(){
	console.log('listening on *:3000');
});

/*
 *
 *	Other Fuctions
 *
 */

function generate_new_map(socket_id) {
	let new_map = new GameOfLife(width, height);
	if (maps.length === 0) {
		// Add if empty, no need to check
		let temp = {
			id: socket_id,
			map: new_map
		};
		maps.push(temp);
	}
	else {
		for (let i = 0; i < maps.length; i ++) {
			if (maps[i].id === socket_id)
				break;
		}
		// If not already in there, add
		let temp = {
			id: socket_id,
			map: new_map
		};
		maps.push(temp);
	}
}

function get_current_map(socket_id) {
	for (let i = 0; i < maps.length; i ++) {
		if (maps[i].id === socket_id) {
			return maps[i];
		}
	}
	console.log('map not found');
	return {};
}

function increment_map(socket_id) {
	for (let i = 0; i < maps.length; i ++) {
		if (maps[i].id === socket_id) {
			let c_Cycle = maps[i].map.cycle();
			maps[i].map.setMap(c_Cycle.map);
		}
	}
	
	// After incrementing emit new
	temp = get_current_map(socket_id).map.map;
	temp = get_minimap(temp);
	io.emit('map', JSON.stringify(temp));
}

function get_minimap(c_map) {
	let minimap = [];
	for (let i = 0; i < c_map.length; i ++) {
		minimap.push({
			state: c_map[i].state,
			index: c_map[i].index
		});
	}
	return minimap;
}

function get_rng_template() {
	let amount = width * height;
	let list = [];
	
	for (let i = 0; i < amount; i ++) {
		list.push(Math.random() >= 0.8);
	}
	
	return list;
}