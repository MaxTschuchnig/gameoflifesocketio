console.log("hello world")

const GameOfLife = require('life-game');

// Map boundings
var width = 5;
var height = 5;

// Template is an array containing just boolean values
var template = [
  false, false, false, false, false,
  false, true, true, false, false,
  false, false, true, false, false,
  false, false, true, false, false
];


var myGame = new GameOfLife(width, height, template);
console.log(myGame.map);

var cycle = myGame.cycle();
myGame.setMap(cycle.map);
console.log(myGame.map);

var cycle = myGame.cycle();
myGame.setMap(cycle.map);
console.log(myGame.map);

var cycle = myGame.cycle();
myGame.setMap(cycle.map);
console.log(myGame.map);